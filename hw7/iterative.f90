program iterative
implicit none
real, dimension(3, 3) :: A
real, dimension(3) :: x, b, theta
real :: sigma
integer :: i, j, k

A(1,1) = 1
A(1,2) = 1
A(1,3) = 2
A(2,1) = 2
A(2,2) = 3
A(2,3) = 5
A(3,1) = 3
A(3,2) = 3
A(3,3) = 6

b = (/ 12, 27, 36/)

x = 0

do k = 1, 100
        do i = 1, 3
                sigma = 0
                do j = 1, 3
                       if (j /= i) then
                                sigma = sigma + A(i,j)*x(j)
                       end if
                end do
        x(i) = (b(i) - sigma) / A(i, i)
        end do
end do

print *, 'Part A: Jacobi Method solution'
print *, x

theta = 0.1

do k = 1, 100
        do i = 1, 3
                sigma = 0
                do j = 1, 3
                        if (j /= i) then
                                sigma = sigma + A(i,j)*theta(j)
                        end if
                end do
        theta(i) = (b(i) - sigma) / A(i,i)
        end do
end do

print *, 'Part B: Gauss-Seidel Method solution'
print *, theta

print *, 'Part C: See above examples'
end program
