module functions

contains

function dotproduct(u, v, n)
integer :: n, i
real :: dotproduct
integer :: u(n), v(n)
do i = 1, n
        dotproduct = dotproduct + u(i)*v(i)
end do
end function dotproduct

function proj(u, v, n)
integer :: proj(n)
integer :: u(n), v(n)
integer :: n
proj = (dotproduct(v, u, n) / dotproduct(u, u, n))*u
end function proj

function normalize(u, n)
integer :: n
integer :: u(n)
double precision :: normalize(n)
normalize = u / (u(1)**2 + u(2)**2 + u(3)**2)**0.5
end function normalize
end module
