program gramschmidt
use functions
implicit none

integer :: i, j
integer, parameter :: n = 3
integer :: v1(n), v2(n), v3(n), u1(n), u2(n), u3(n)
double precision :: e1(n), e2(n), e3(n)

v1 = (/3, 0, 0/)
v2 = (/1, 2, 0/)
v3 = (/1, 2, 5/)

u1 = v1
u2 = v2 - proj(u1, v2, n)
u3 = v3 - proj(u1, v3, n) - proj(u2, v3, n)
print *, "Orthogonal Vectors"
print *, u1
print *, u2
print *, u3
print *, "Orthonormal Vectors"
print *, normalize(u1, n)
print *, normalize(u2, n)
print *, normalize(u3, n)      
end program gramschmidt
