program integrate
use routines
implicit none
integer, parameter :: x_start = 0, x_end = 1
integer            ::  i
integer, dimension(7) :: N
double precision               :: isum, M, err

N(1) = 11
N(2) = 101
N(3) = 1001
N(4) = 10001
N(5) = 100001
N(6) = 1000001
N(7) = 10000001

do i=1, 7
        M = N(i)-1
        h = (x_end - x_start)/M
        isum = 0
                do xi=x_start, x_end, h
                        isum = isum + h*midp(xi, h)
                end do
        err = isum - .125
        print *, "N =", N(i) 
        print *, "Midpoint sum =", isum, "Midpoint Error =", err
                isum = 0
                err = 0
                do xi=x_start, x_end, h
                        isum = isum + (h/2)*trap(xi, h)
                end do
        err = isum - .125
        print *, "Trapezoid sum =", isum, "Trapezoid Error =", err
                isum = 0
                err = 0
                do xi=x_start, x_end, h

                        isum = isum + (h/6)*simp(xi, h)
                end do
                err = isum - .125
        print *,"Simpson sum =", isum, "Simpson Error =", err
                err = 0
                isum = 0 
end do
end program 
