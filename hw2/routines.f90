module routines

        real :: xi, h

contains

real function midp(xi, h)
        midp = (xi+h*0.5d0)**7
end function 

real function trap(xi, h)
        trap = xi**7 + (xi+h)**7
end function

real function simp(xi, h)
        simp = (xi-h)**7 + 4*xi**7 + (xi+h)**7
end function
end module routines
