program normal
implicit none

integer, parameter :: num = 100000
integer :: i, x, s
double precision :: u(num), y(num)

open(unit = 1, file = 'rand_normal.dat')

do i = 1, num
        call random_number(u(i))
        call random_number(y(i))
        y(i) = -5 +10*y(i)
        s = -y(i)*y(i)/2
        if (u(i) <= s) then 
                x = x+1
                x = y(i)
        end if
        if (x == 100000) go to 20 
write(1,*) y(i)
end do
20 continue
close(1)

end program normal
