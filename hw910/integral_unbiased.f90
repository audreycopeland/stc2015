program unbiased
implicit none

integer, parameter :: num = 100000
integer :: i
double precision :: sum
double precision :: u(num), x(num)

open(unit = 1, file = 'integral_unbiased.dat')

do i = 1, num
        call random_number(u(i))
        x(i) = 10*u(i) - 5
        sum = 0
        sum = sum + exp(-x(i)**2/2)*(cos(x(i)) + 5)
end do

print *, num, sum

end program unbiased
