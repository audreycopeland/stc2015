program sample
implicit none
integer, parameter :: num = 100000
double precision :: x(num)
integer :: i

open(unit = 1, file = 'rand_uniform.dat')
do i = 1, num
call random_number(x(i))
write(1, *) 10*x(i)-5
end do


end program sample
