program importance
implicit none

integer, parameter :: num = 100000
integer :: i, x, s
double precision :: u(num), y(num)
double precision :: sum

do i = 1, num
        call random_number(u(i))
        call random_number(y(i))
        y(i) = -5 +10*y(i)
        s = -y(i)*y(i)/2
        if (u(i) <= s) then 
                x = x+1
                x = y(i)
        end if
        if (x == 100000) go to 20 
end do
20 continue
sum = 0
do i = 1, num
        sum = sum + (-y(i)**2/2)*(cos(y(i)) + 5)
end do
print *, num, sum

end program importance
