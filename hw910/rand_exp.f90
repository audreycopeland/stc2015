program exp
implicit none

integer, parameter :: num = 100000
integer :: i
double precision :: x(num), y(num)

open(unit = 1, file = 'rand_exp.dat')

do i = 1, num
        call random_number(x(i))
        y(i) = -3*log(x(i))
        write(1, *) y(i)
end do
close(1)

end program exp
