subroutine isqrt(a, n)
        integer :: n,i
        real    :: a(n)
!                       Vector inverse square
!                       Someone advised to 
!                       making two separate loops.
        do i=1,n; a(i) = 1.0e0/sqrt(a(i)); end do
end subroutine
