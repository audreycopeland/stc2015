program project
use functions
implicit none
integer :: num, i, j, path, theecc, rad, dia
real :: avgpath
double precision, allocatable :: input(:,:)
integer, allocatable :: answer(:,:), other(:,:)
character(len = 10) :: fmt
character(len = 30) :: filename, outputfile

call getarg(1, filename) 
call getarg(2, outputfile)

fmt = "(I4)"
num = 0

open(unit=1, file=filename)
do 
        read(1,*, end=10)
        num = num + 1
end do
10 close(1)

print*,'Dimension:', num

allocate(input(num,num))
allocate(other(num,num))
allocate(answer(num,num))

open(unit=1, file=filename)
do i = 1, num
         read(1,*) other(:,(i))
end do 
close(1)
input = dble(other)

open(unit=2, file=outputfile)

answer = Seidel(input, num)

do i = 1, num
        do j = 1, num
                write(2, fmt, advance = 'no') answer(i, j)
        end do
        write(2,*) ''
end do

path = mindistance(answer, num, 1, 2)
print *, 'Minimum path between two nodes:', path
avgpath = avgnpdistance(answer, num)
print *, 'Average distance between any two nodes:', avgpath
theecc = ecc(answer, num, 2)
print *, 'Eccentricity of a node:', theecc
rad = radius(answer, num)
print *, 'Radius:', rad
dia = diameter(answer, num)
print *, 'Diameter:',  dia

close(2)
end program
