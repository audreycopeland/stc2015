program reuse
implicit none

integer, parameter :: n = 2048
integer :: i, j, k
double precision, dimension(n, n) :: A, B, C
double precision :: sum

do i=1, n
        do j=1, n
                A(i, j) = 1.0
                B(i, j) = 1.0
        end do
end do

do i=1, n
        do j=1, n
                sum = 0
                do k=1, n
                        sum = sum + A(i,k)*B(k,j)
                end do
        C(i, j) = sum
        end do
end do

end program

