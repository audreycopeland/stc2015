program noreuse

implicit none

integer :: i, j, k
integer, parameter :: n = 2048
double precision, dimension(n,n) :: A, B, C

do i=1, n
        do j=1, n
                A(i, j) = 1.0
                B(i, j) = 1.0
        end do
end do

do i=1, n
        do j=1, n
                C(i, j) = 0
                do k=1, n
                        C(i, j) = C(i, j) + A(i,k)*B(k, j)
                end do
        end do
end do
end program
