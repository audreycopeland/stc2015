subroutine operator(f, s)
integer, parameter :: M = 8, NRHS = 8
integer, parameter :: LDA = M, LDB = M
integer, dimension(M) :: IPIV
double precision, parameter :: alp = 1.0, bet = 0.0
double precision, dimension(LDA, M) :: A
double precision, dimension(LDB, NRHS) :: C
double precision, dimension(8) :: xi, fi, f, W
double precision, dimension(8,8) :: b
double precision :: sum, x, D
integer :: i, j, k, INFO
character(LEN=*) :: s

xi = (/ 0.0, 0.142857142857143, 0.285714285714286, 0.428571428571429, &
0.571428571428571, 0.714285714285714, 0.857142857142857, 1.0 /) 

fi = (/ 0.0, 1.214265678902012E-006, 1.554260068994575E-004, &
2.655599039758700E-003, 1.989452888313056E-002, 9.486450616421972E-002, &
0.339916677089114, 1.0 /) 

C = 0.0
do i = 1, 8
        C(i, i) = 1.0
end do
 
x = 0.5

do k = 0, 8
        do j = 0, 8
                b((k+1), (j+1)) = xi(j+1)**k 
        end do 
end do

print *, "B"
print *, ''
write(*, "(8(E16.6, x))"), b
print *, ''
call dgesv(M, NRHS, b, LDA, IPIV, C, LDB, INFO)
print *, "B^-1" 
print *, ''
write(*, "(8(E16.6, x))"), C
print *, ''
call dgemm('n', 'n', 8, 8, 8, alp, C, 8, f, 8, bet, W, 8)
print *, "Weights for ", s
print *, ''
print *, "W(0)", W(1)
print *, "W(1)", W(2)
print *, "W(2)", W(3)
print *, "W(3)", W(4)
print *, "W(4)", W(5)
print *, "W(5)", W(6)
print *, "W(6)", W(7)
print *, "W(7)", W(8)
print *, ''

call dgemm('t', 'n', 1, 1, 8, alp, W, 8, fi, 8, bet, D, 1) 
print *, "Checking the Accuracy for ", s
print *, ''
print *, "x    numerical value         analytical value            absolute &
error"
print *, "0.5", D, f(8), D-f(8)
print *, ''
return
end
