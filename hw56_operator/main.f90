program main
double precision, dimension(8) :: f
character(LEN=30) :: s

s = 'Interpolation'
f = (/ 1.0, 0.5, 0.25, 0.125, 0.0625, 0.03125, 0.015625, 0.0078125 /)
call operator(f, s)

s = "First Derivative"
f = (/ 0.0, 1.0, 1.0, 0.75, 0.5, 0.3125, 0.1875, 0.109375 /)
call operator(f, s)

s = "Second Derivative"
f = (/0.0, 0.0, 2.0, 3.0, 3.0, 2.5, 1.875, 1.3125 /)
call operator(f, s)

s = "Integration from 0 to 0.5"
f = (/0.5, 0.125, 0.0416666666667, 0.015625, 0.00625, 0.0026041667, &
0.0011160714, 4.8828125E-004 /)
call operator(f, s)

end program
