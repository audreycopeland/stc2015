program linear_inverse

integer, parameter :: M = 5, NRHS = 5
integer, parameter :: LDA = M, LDB = M
integer :: INFO
double precision, dimension(LDA, M) :: A, C, D, G
double precision, dimension(LDB, NRHS) :: b
integer, dimension(M) :: IPIiV
double precision :: alp = 1.0, bet= 0.0


A(1,1) = 1D0
A(1,2) = 0.5D0
A(1,3) = 0.33333333333333D0
A(1,4) = 0.25D0
A(1,5) = 0.2D0
A(2,1) = 0.5D0
A(2,2) = 0.33333333333333D0
A(2,3) = 0.25D0
A(2,4) = 0.2D0
A(2,5) = 0.16666666666667D0
A(3,1) = 0.33333333333333D0
A(3,2) = 0.25D0
A(3,3) = 0.2D0
A(3,4) = 0.16666666666667D0
A(3,5) = 0.14285714285714D0
A(4,1) = 0.25D0
A(4,2) = 0.2D0
A(4,3) = 0.16666666666667D0
A(4,4) = 0.14285714285714D0
A(4,5) = 0.125D0
A(5,1) = 0.2D0
A(5,2) = 0.16666666666667D0
A(5,3) = 0.14285714285714D0
A(5,4) = 0.125D0
A(5,5) = 0.11111111111111D0

C = A

b(1,1) = 1
b(1,2) = 0
b(1,3) = 0
b(1,4) = 0
b(1,5) = 0
b(2,1) = 0
b(2,2) = 1
b(2,3) = 0
b(2,4) = 0
b(2,5) = 0
b(3,1) = 0
b(3,2) = 0
b(3,3) = 1
b(3,4) = 0
b(3,5) = 0
b(4,1) = 0
b(4,2) = 0
b(4,3) = 0
b(4,4) = 1
b(4,5) = 0
b(5,1) = 0
b(5,2) = 0
b(5,3) = 0
b(5,4) = 0
b(5,5) = 1

print *, 'A'
write(*, "(5(E16.6, x))") A

call dgesv(M, NRHS, A, LDA, IPIV, b, LDB, INFO)

print *, 'A^-1'
write(*, "(5(E16.6, x))"), b

call dgemm('n', 'n', 5, 5, 5, alp, C, 5, b, 5, bet, D, 5) 

print *, 'AA^-1'
write(*, "(5(E16.6, x))"), D

call dgemm('n', 'n', 5, 5, 5, alp, b, 5, C, 5, bet, G, 5)

print *, 'A^-1A'
write(*, "(5(E16.6))" ), G

end program
