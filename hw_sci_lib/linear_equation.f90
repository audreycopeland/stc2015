program linear_equation

integer, parameter :: N = 3, NRHS = 1
integer, parameter :: LDA = N, LDB = N
integer :: INFO
double precision, dimension(LDA, N) :: A
double precision, dimension(LDB, NRHS) :: b
integer, dimension(N) :: IPIV

A(1,1) = 6
A(1,2) = -2
A(1,3) = 2
A(2,1) = 12
A(2,2) = -8
A(2,3) = 6
A(3,1) = 3
A(3,2) = -13
A(3,3) = 3

b(1,1) = 16
b(2,1) = 26
b(3,1) = -19

call dgesv(N, NRHS, A, LDA, IPIV, b, LDB, INFO)

print *, 'x1 =', b(1,1)
print *, 'x2 =', b(2,1)
print *, 'x3 =', b(3,1)

end program
